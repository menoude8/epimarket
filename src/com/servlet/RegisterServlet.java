package com.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.Address;
import com.app.Client;

/**
 * Servlet implementation class RegisterServlet
 */

@WebServlet("/register")

public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PERSISTANCE_UNIT = "epiMarket";
	private EntityManager em = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT).createEntityManager();   
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().invalidate();
		response.sendRedirect("home.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String streetname = request.getParameter("streetnameB");
		String streetnumber = request.getParameter("streetnumberB");
		String city = request.getParameter("cityB");
		String zipcode = request.getParameter("zipcodeB");
		String country = request.getParameter("countryB");
		
		if(!streetname.isEmpty() && streetname != null && !streetnumber.isEmpty() && streetnumber != null && !city.isEmpty() && city != null
				&& !zipcode.isEmpty() && zipcode != null && !country.isEmpty() && country != null){
			
			Address BillingA = new Address(streetnumber, streetname, city, zipcode, country);
			streetname = request.getParameter("streetnameD");
			streetnumber = request.getParameter("streetnumberD");
			city = request.getParameter("cityD");
			zipcode = request.getParameter("zipcodeD");
			country = request.getParameter("countryD");
			
			if(!streetname.isEmpty() && streetname != null && !streetnumber.isEmpty() && streetnumber != null && !city.isEmpty() && city != null
					&& !zipcode.isEmpty() && zipcode != null && !country.isEmpty() && country != null){
				Address DeliveryA = new Address(streetnumber, streetname, city, zipcode, country);
				
				String login = request.getParameter("login");
				String password = request.getParameter("password");
				String First = request.getParameter("firstname");
				String Middle = request.getParameter("middlename");
				String Last = request.getParameter("lastname");
				String Mail = request.getParameter("mail");

					if(login != null && password != null && !login.isEmpty() && !password.isEmpty()
					&& 	First != null && !First.isEmpty() && Middle != null && !Middle.isEmpty()
					&& Mail != null && !Mail.isEmpty() && Last != null && !Last.isEmpty()){
					EntityTransaction tx = em.getTransaction();
					tx.begin();
					em.persist(DeliveryA);
					em.persist(BillingA);
					em.getTransaction().commit();
					em.getTransaction().begin();
					Client currentUser = new Client();
					currentUser.setLogin(login);
					currentUser.setPass(password);
					currentUser.setFirstName(First);
					currentUser.setMiddleName(Middle);
					currentUser.setLastName(Last);
					currentUser.setMail(Mail);
					currentUser.setBillingAddress(BillingA);
					currentUser.setDeliveryAddress(DeliveryA);
					em.persist(currentUser);
					tx.commit();
					request.getSession().setAttribute("user", currentUser);
					request.getRequestDispatcher("content.jsp").forward(request, response);
			}
		}
		
		} else {
			response.sendRedirect("home.jsp");
		}
	}

}
