package com.servlet;

import java.io.IOException;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.*;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PERSISTANCE_UNIT = "epiMarket";
	private EntityManager em = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT).createEntityManager();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().invalidate();
		response.sendRedirect("home.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String password = request.getParameter("password");

		if(login != null && password != null && !login.isEmpty() && !password.isEmpty()){

			Query req = em.createNativeQuery("select ID from Client where LOGIN = '" + login + "' and PASS = '" + password + "'");
			@SuppressWarnings("unchecked")
			java.util.List<String> id = req.getResultList();
			if (!id.isEmpty())
			{
				Client currentUser = em.find(Client.class, id.get(0));
				
				request.getSession().setAttribute("user", currentUser);
				request.getRequestDispatcher("content.jsp").forward(request, response);
			}
			else {
				response.sendRedirect("home.jsp");
			}
		} else {
			response.sendRedirect("home.jsp");
		}
	}

}
