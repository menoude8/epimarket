package com.servlet;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.Cart;
import com.app.Client;
import com.app.OrderLine;
import com.app.Product;

/**
 * Servlet implementation class ViewProductServlet
 */
public class ViewProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PERSISTANCE_UNIT = "epiMarket";
	private EntityManager em = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT).createEntityManager();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().invalidate();
		response.sendRedirect("home.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nb = null;
		nb = request.getParameter("nbP");
		Client CurrentUser = (Client) request.getSession().getAttribute("user");
		if (nb != null && !nb.isEmpty())
		{
			Cart		panier = new Cart(CurrentUser);
			List<OrderLine>	List = null;
			Integer i = Integer.parseInt(nb);
			for (Integer k = 0; i != k; k++)
			{	
				String id = null;
				id = request.getParameter(k.toString());
				if (!id.isEmpty() && id != null)
					{
						OrderLine tmp  = new OrderLine(em.find(Product.class, id), "1");
						List.add(tmp);
					}
			}
			panier.setList(List);
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			em.persist(panier);
			tx.commit();
			response.sendRedirect("content.jsp");
		}
		else
		response.sendRedirect("home.jsp");
	}

}
