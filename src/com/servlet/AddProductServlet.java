package com.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.Category;
import com.app.Product;

/**
 * Servlet implementation class AddProductServlet
 */
public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PERSISTANCE_UNIT = "epiMarket";
	private EntityManager em = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT).createEntityManager();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().invalidate();
		response.sendRedirect("content.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String Cat = request.getParameter("category");
		String Type = request.getParameter("type");
		String Designation = request.getParameter("designation");
		String Price = request.getParameter("price");
		Category CCAT;
		
		if (Cat != null  && !Cat.isEmpty() && Type != null && !Type.isEmpty() && Designation != null && !Designation.isEmpty() && Price != null && !Price.isEmpty())
		{
			Query req = em.createNativeQuery("select ID from Category where NAME = '" + Cat + "'");
			@SuppressWarnings("unchecked")
			java.util.List<String> id = req.getResultList();
			if (!id.isEmpty())
			{
				CCAT = em.find(Category.class, id.get(0));
			}
			else
			{
				EntityTransaction tx = em.getTransaction();
				tx.begin();
				CCAT = new Category(Cat);
				em.persist(CCAT);
				tx.commit();
			}
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			Product Prod = new Product(CCAT, Type, Designation, Price);
			em.persist(Prod);
			tx.commit();
			response.sendRedirect("content.jsp");
		}
		else
			response.sendRedirect("content.jsp");
	}
}
