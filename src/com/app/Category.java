package com.app;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class Category
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String name;

	public Category()	{}

	public Category(String name)
	{
		ssetName(name);
	}

	public String toString()
	{
		return "Category instance, name= " + name;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//Getters & Setters
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	public String getName()					{return name;}
	
	public void ssetName(String name)		{this.name = name;}

	public Integer getId() {return id;}

	public void setId(Integer id) {this.id = id;}
}