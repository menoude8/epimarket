package com.app;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer		id;
	private String 		streetNumber;
	private String 		streetName;
	private String 		city;
	private String 		zipCode;
	private String 		country;
	
	public Address(){}
	
	public Address(String sNumber, String sName, String _city, String zCode, String _country)
	{
		streetNumber = sNumber;
		streetName = sName;
		city = _city;
		zipCode = zCode;
		country = _country;
	}

	@Override
	public String toString()
	{
		return "Address instance, id= " + id + ", streetNumber= " + streetNumber + ", streetName= " + streetName + ", city= " + city + ", zipCode= " + zipCode + ", country= " + country;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//Getters & Setters
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	public String getStreetNumber()						{return streetNumber;}
	public String getStreetName()						{return streetName;}
	public String getCity()								{return city;}
	public String getZipCode()							{return zipCode;}
	public String getCountry()							{return country;}

	public void setStreetNumber(String streetNumber)	{this.streetNumber 	= streetNumber;}
	public void setStreetName(String streetName)		{this.streetName 	= streetName;}
	public void setCity(String city)					{this.city 			= city;}
	public void setZipCode(String zipCode)				{this.zipCode 		= zipCode;}
	public void setCountry(String country)				{this.country		= country;}

	public Integer getId() 								{return id;}
	public void setId(Integer id) 						{this.id = id;}
	
}
