package com.app;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private	Integer	id;
	
	private Category category;
	private String designation, type;
	private String price;

	public Product() {
	}

	public void sValues(Category cat, String type, String designation, String price) {
		this.category = cat;
		this.type = type;
		this.designation = designation;
		this.price = price;
	}
	
	public Product(Category cat, String type, String designation, String price) {
		sValues(cat, type, designation, price);
	}

	public String toString()
	{
		return "designation= " + designation + ", type= " + type + ", price= " + price + ", Category= " + category.getName();
	}
	
///////////////////////////////////////////////////////////////
////////////////     GETTER and SETTEUR  //////////////////////
///////////////////////////////////////////////////////////////	
	
	public Category getCategory() {return category;}
	public String getDesignation() { return designation; }
	public String getType() { return type; }
	public String getPrice() { return price; }
	public Integer getId() {return id;}
	
	public void setDesignation(String designation) { this.designation = designation;}
	public void setType(String type) { this.type = type; }
	public void setPrice(String price) { this.price = price; }

	public void setId(Integer id) {this.id = id;}
	public void setCategory(Category category) {this.category = category;}
	
}
