package com.app;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Client {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer		id;

	private Address		billingAddress;
	private Address		deliveryAddress;
	
	
	private String		login;
	private String		pass;
	private String		mail;
	private String		firstName;
	private String		middleName;
	private String		lastName;


	public Client()
	{
		this.login = "anonyme";
	    this.pass = "default";
	}
	

	public Client(Address bAddress, Address dAddress, String fName, String mName, String lName, String mail)
	{
		billingAddress = bAddress;
		deliveryAddress = dAddress;
		firstName = fName;
		middleName = mName;
		lastName = lName;
		this.login = "anonyme";
	    this.pass = "default";
	    this.mail = mail;
	}
	
	public String toString()
	{
		return "Client instance: id= " + id +  ", firstName=" 	+ firstName + ", middleName=" + middleName + ", lastName=" + lastName 
												+ "\r\n\t\tbillingAddress: " 	+ ((billingAddress != null)? 	billingAddress.toString() 	: "null") 
		 										+ "\r\n\t\tdeliveryAddress: " 	+ ((deliveryAddress != null)? 	deliveryAddress.toString() : "null");
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//Getters & Setters
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	public Address getBillingAddress()						{return billingAddress;}
	public Address getDeliveryAddress()						{return deliveryAddress;}
	public String getFirstName()							{return firstName;}
	public String getMiddleName()							{return middleName;}
	public String getLastName()								{return lastName;}
	
	public void setFirstName(String firstName)				{this.firstName 		= firstName;}
	public void setMiddleName(String middleName)			{this.middleName 		= middleName;}
	public void setLastName(String lastName)				{this.lastName 			= lastName;}
	
	public Integer getId() {return id;}
	public void setId(Integer id) {this.id = id;}
	public void setBillingAddress(Address billingAddress)	{this.billingAddress 	= billingAddress;}
	public void setDeliveryAddress(Address deliveryAddress)	{this.deliveryAddress 	= deliveryAddress;}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLogin() {
		return login;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPass() {
		return pass;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMail() {
		return mail;
	}
}
